var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;
var messageSchema = new Schema(
  {
    roomChatID: ObjectId,
    senderID: ObjectId,
    msg: String,
    createAt: String
  },
  { versionKey: false }
);

module.exports = mongoose.model('Message', messageSchema);
