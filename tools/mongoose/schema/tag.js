var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var tagSchema = new Schema(
    {
        name: String,
        isAproved: Boolean
    },
    { versionKey: false }
);

module.exports = mongoose.model('Tag', tagSchema);
