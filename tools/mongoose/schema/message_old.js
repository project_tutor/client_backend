var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;
var messageSchema = new Schema(
  {
    senderID: ObjectId,
    receiverID: ObjectId,
    msg: String,
    createAt: Date
  },
  { versionKey: false }
);

module.exports = mongoose.model('Message', messageSchema);
