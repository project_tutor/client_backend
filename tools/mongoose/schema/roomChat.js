var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var roomChatSchema = new Schema(
  {
    member: [ObjectId]
  },
  { versionKey: false }
);

module.exports = mongoose.model('RoomChat', roomChatSchema);
