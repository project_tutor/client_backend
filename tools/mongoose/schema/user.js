var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var userSchema = new Schema(
  {
    email: String,
    password: String,
    fullname: String,
    imageURL: String,
    phone: String,
    location: {
      address: String,
      city: String
    },
    role: String,
    isActived: Boolean,
    isReadEmail: Boolean,
    isLocked: Boolean,
    money: String,
    follow: [ObjectId],
    isSocial: Boolean,
  },
  { versionKey: false }
);

module.exports = mongoose.model('User', userSchema);
