const { gql } = require('apollo-server-express');

module.exports = gql`
  # Comments in GraphQL strings (such as this one) start with the hash (#) symbol.

  # This "Book" type defines the queryable fields for every book in our data source.
  type User {
    _id: ID
    fullname: String
    email: String
    imageURL: String
    location: Location
    phone: String
    role: String
    isActived: Boolean
    isReadEmail: Boolean
    isLocked: Boolean
    money:String
    follow: [ID]
    isSocial: Boolean
  }

  type Location {
    address: String
    city: String
  }

  input LocationInput {
    address: String
    city: String
  }

  input UserInput {
    fullname: String
    imageURL: String
    location: LocationInput
    phone: String
    education: [EducationInput]
    employeeHistory: [EmployeeHistoryInput]
    skill: [TagInput]
    hourlyRate: String
    isSocial: Boolean
  }

  input LoginInput {
    email: String
    password: String
  }

  type Token {
    token: String
    user: User
  }

  enum Role {
    STUDENT
    TEACHER
  }

  input RegisterInput {
    fullname: String
    email: String
    password: String
    role: Role
    imageURL: String
    isSocial: Boolean
  }

  # Tag
  type Tag{
    _id: ID
    name: String,
    isAproved: Boolean
  }

  input TagInput {
    name: String
  }

  # Post
  type Post{
    _id: ID,
    teacher: User,
    title: String,
    overview: String,
    hourlyRate: String,
    skill: [Tag],
    education: [Education],
    employeeHistory: [EmployeeHistory],
    isVisable: Boolean,
    rate: [Int],
    students: [User]
  }

  input PostInput{
    # teacher: User,
    title: String,
    overview: String,
    hourlyRate: String,
    skill: [TagInput],
    education: [EducationInput],
    employeeHistory: [EmployeeHistoryInput],
    # isVisable: Boolean,
    # rate: Number,
    # students: [User]
  }
  
  type Education {
    name: String
    timeStart: String
    timeEnd: String
    degree: String
    description: String
  }

  input EducationInput {
    name: String
    timeStart: String
    timeEnd: String
    degree: String
    description: String
  }

  type EmployeeHistory {
    title: String
    workPlace: String
    timeStart: String
    timeEnd: String
    description: String
  }

  input EmployeeHistoryInput {
    title: String
    workPlace: String
    timeStart: String
    timeEnd: String
    description: String
  }

  # File
  type File {
    filename: String!
    mimetype: String!
    encoding: String!
    path: String!
  }

  # Feedback
  type Feedback{
    content: String,
    rate: Int,
    createAt: String
  }

  input FeedbackInput{
    content: String,
    rate: Int,
  }

  type FeedbackWithStudent{
    student: User
    content: String,
    rate: Int,
    createAt: String
  }

  # Contract
  type Contract{
    _id: ID
    teacher: User,
    student: User,
    state: STATE_CONTRACT,
    title: String,
    description: String,
    feeHourly: String,
    timeStudy: Int,
    skill: Tag,
    finishAt: String,
    feedbacks: [Feedback]
  }

  input ContractInput{
    title: String,
    description: String,
    feeHourly: String,
    timeStudy: Int,
    skill: ID
  }

  enum STATE_CONTRACT {
    PENDING
    WAITINGPAY
    AGREE
    DISAGREE
    FINISH
    COMPLAINING
    STUDENTWIN 
    TEACHERWIN
  }

  # Complaint
  type Complaint{
    _id: ID
    contract: Contract
    studentMessage: String
    teacherMessage: String,
    state: STATE_COMPLAINT,
    whoAccepted: WHOACCEPTED_COMPLAINT,
    createAt: String
  }

  enum STATE_COMPLAINT {
    PENDING
    PROCESSED
  }

  enum WHOACCEPTED_COMPLAINT {
    STUDENT
    TEACHER
  }
  

  # Chat
  type Message {
    _id: ID
    roomChatID: ID
    msg: String,
    sender: User
    createAt: String
  }

  type RoomChat{
    _id: ID
    member: [User]
    allMessage: [Message]
  }

  type Query {
    # User
    users: [User]
    userLogin: User

    # Tag
    tags: [Tag]
    tagsAproved: [Tag]

    # Post
    myPost: Post
    posts: [Post]
    followPost: [Post]
    studiedPost: [User]

    # Contract
    myContracts: [Contract]
    getFeedbacksByPostID(postID: ID!): [FeedbackWithStudent]

    # Complaint
    myComplaints: [Complaint]

    # File
    uploads: [File]

    # Chat [RoomChat]
    myRoomChat: [RoomChat]

    # Home
    topTeachers: [Post]
    topTags: [Tag]
  }

  type Mutation {
    # Login
    login(input: LoginInput!): Token

    # Register
    register(input: RegisterInput!): Boolean
    confirmEmail(token: String!): Boolean
    forgotPassword(email: String!): Boolean
    resetPassword(token: String!, newPassword: String!): Boolean

    # User
    addUser(input: UserInput!): User
    editUser(userID: ID!, input: UserInput!, imageFile: Upload): User
    changePassword(oldPassword: String!, newPassword: String!): Boolean

    # Tag
    addTag(name: String!): Boolean

    # Post
    writePost(input: PostInput!): Boolean
    followingPost(postId: ID!): Boolean
    cancelFollowPost(postId: ID!): Boolean

    # File
    uploadFile(file: Upload!): File!

    # Money
    editMoney(money: String): Boolean

    # Contract
    registerContract(teacherID: ID!, input: ContractInput!): Boolean
    teacherAgreeContract(contractID: ID!): Boolean
    teacherDisagreeContract(contractID: ID!): Boolean
    studentPayContract(contractID: ID!): Boolean
    studentComplaintContract(contractID: ID!, studentMessage: String!): Boolean
    studentFinishContract(contractID: ID!, feedback: FeedbackInput): Boolean

    # Complaint
    teacherWriteMessageInComplaint(complaintID: ID!, teacherMessage: String!): Boolean
    studentUPdateComplaint(complaintID: ID!, studentMessage: String!): Boolean
    
    # Messages
    sendMessage(receiverID: ID!, msg: String): Boolean
  }

  type Subscription {
    subUsers: User
    onMessages: Message
  }
`;
