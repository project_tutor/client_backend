const userSchema = require('@schema/user');
const contractSchema = require('@schema/contract');
const complaintSchema = require('@schema/complaint');

module.exports = {
  Query: {
    myComplaints: async (parent, args, context, info) => {
      if (!context.user) {
        throw new AuthenticationError('Authentication token is invalid');
      }
      const user = await userSchema.findOne({ email: context.user.email });
      const myContracts = await contractSchema.find({
        $or: [{ teacherID: user._id }, { studentID: user._id }],
        state: ['COMPLAINING', 'STUDENTWIN', 'TEACHERWIN']
      });

      const myArrayContractID = myContracts.map((item) => item._id);


      const myComplaints = await complaintSchema.find({
        contractID: myArrayContractID
      });

      const myComplaintsReturn = [];
      for (let i = 0; i < myComplaints.length; i++) {
        const theContract = await contractSchema.findOne({ _id: myComplaints[i].contractID });

        myComplaintsReturn.push({
          _id: myComplaints[i]._id,
          studentMessage: myComplaints[i].studentMessage,
          teacherMessage: myComplaints[i].teacherMessage,
          state: myComplaints[i].state,
          whoAccepted: myComplaints[i].whoAccepted,
          createAt: myComplaints[i].createAt,
          contract: theContract
        });
      }

      return myComplaintsReturn;
    },
  },
  Mutation: {
    teacherWriteMessageInComplaint: async (parent, args, context, info) => {
      if (!context.user) {
        throw new AuthenticationError('Authentication token is invalid');
      }
      const { complaintID, teacherMessage } = args;
      const user = await userSchema.findOne({ email: context.user.email });
      if (user.role !== 'TEACHER') {
        throw new Error("Phải là giáo viên mới thực hiện được!");
      }
      await complaintSchema.updateOne({ _id: complaintID }, { teacherMessage });

      return true;
    },
    studentUPdateComplaint: async (parent, args, context, info) => {
      if (!context.user) {
        throw new AuthenticationError('Authentication token is invalid');
      }
      const { complaintID, studentMessage } = args;
      const user = await userSchema.findOne({ email: context.user.email });
      await complaintSchema.updateOne({ _id: complaintID }, { studentMessage });

      return true;
    },
  },
};
