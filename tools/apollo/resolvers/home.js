const tagSchema = require('@schema/tag');
const userSchema = require('@schema/user');
const postSchema = require('@schema/post');
const contractSchema = require('@schema/contract');


module.exports = {
  Query: {
    topTeachers: async (parent, args, context, info) => {
      let teachers =  await userSchema.find({ role: 'TEACHER' });
      teachers = teachers.sort((a, b)=>(parseInt(a.money, 0) > parseInt(b.money, 0) ? -1 : 1 ));
      const result = [];
      const n = teachers.length > 9 ? 9 : teachers.length;
      for ( let i = 0; i < n; i++) {
        const postOfUser = await postSchema.find({ teacherID: teachers[i]._id });
        if(postOfUser.length != 0) {
          const tags = await tagSchema.find({ _id: { $in: postOfUser[0].skill } });

          result.push({
            _id: postOfUser[0]._id,
            teacher: teachers[i],
            hourlyRate: postOfUser[0].hourlyRate,
            skill: tags,
            rate: postOfUser[0].rate,
          });
        }
      }
      return result
      
    },
    topTags: async (parent, args, context, info) => {
      let tags = await tagSchema.find({ isAproved: true });
      let result = [] 
           
      for (let i = 0; i < tags.length; i++) {
        const temp = await contractSchema.find({ skill: tags[i]._id });
        result.push({ counter: temp.length, _id: tags[i]._id, name: tags[i].name });
      }      
      result = result.sort((a, b) => (a.counter > b.counter ? -1 : 1 ));
      return result;
    }
  },
  
};