const userSchema = require('@schema/user');
const contractSchema = require('@schema/contract');
const tagSchema = require('@schema/tag');
const complaintSchema = require('@schema/complaint');
const postSchema = require('@schema/post');

module.exports = {
  Query: {
    myContracts: async (parent, args, context, info) => {
      if (!context.user) {
        throw new AuthenticationError('Authentication token is invalid');
      }
      const user = await userSchema.findOne({ email: context.user.email });
      const arrContractReturn = [];
      if (user.role === "STUDENT") {
        const contract = await contractSchema.find({ studentID: user._id });
        for (let i = 0; i < contract.length; i++) {
          // Lấy teacher
          const teacher = await userSchema.findOne({ _id: contract[i].teacherID });

          // Lây skill
          const skill = await tagSchema.findOne({ _id: contract[i].skill });
          arrContractReturn.push({
            _id: contract[i]._id,
            teacher,
            student: user,
            state: contract[i].state,
            title: contract[i].title,
            description: contract[i].description,
            feeHourly: contract[i].feeHourly,
            timeStudy: contract[i].timeStudy,
            skill
          });
        }
      } else {
        const contract = await contractSchema.find({ teacherID: user._id });
        for (let i = 0; i < contract.length; i++) {
          // Lấy teacher
          const student = await userSchema.findOne({ _id: contract[i].studentID });

          // Lây skill
          const skill = await tagSchema.findOne({ _id: contract[i].skill });
          arrContractReturn.push({
            _id: contract[i]._id,
            teacher: user,
            student,
            state: contract[i].state,
            title: contract[i].title,
            description: contract[i].description,
            feeHourly: contract[i].feeHourly,
            timeStudy: contract[i].timeStudy,
            finishAt: contract[i].finishAt,
            skill
          });
        }
      }
      return arrContractReturn;
    },
  },
  Mutation: {
    registerContract: async (parent, args, context, info) => {
      if (!context.user) {
        throw new AuthenticationError('Authentication token is invalid');
      }
      const { teacherID, input } = args;
      const student = await userSchema.findOne({ email: context.user.email });
      if (student.role !== "STUDENT") {
        throw new Error("Phải là học sinh mới đăng ký được!");
      }

      const contractExist = await contractSchema.findOne({ teacherID, studentID: student._id, state: { $in: ["PENDING", "AGREE", "COMPLAINING", "WAITINGPAY"] } });

      if (contractExist) {
        throw new Error("Hợp đồng đã tồn tại!");
      }

      // const teacher = await userSchema.findOne({ _id: teacherID });
      await new contractSchema({ teacherID, studentID: student._id, state: "PENDING", ...input }).save();

      return true;
    },
    teacherAgreeContract: async (parent, args, context, info) => {
      if (!context.user) {
        throw new AuthenticationError('Authentication token is invalid');
      }
      const user = await userSchema.findOne({ email: context.user.email });
      if (user.role !== 'TEACHER') {
        throw new Error('Phải là giáo viên mới thực hiện được chức năng này!');
      }
      const { contractID } = args;
      await contractSchema.updateOne({ _id: contractID }, { state: "WAITINGPAY" });
      return true;
    },
    teacherDisagreeContract: async (parent, args, context, info) => {
      if (!context.user) {
        throw new AuthenticationError('Authentication token is invalid');
      }
      const user = await userSchema.findOne({ email: context.user.email });
      if (user.role !== 'TEACHER') {
        throw new Error('Phải là giáo viên mới thực hiện được chức năng này!');
      }
      const { contractID } = args;
      await contractSchema.updateOne({ _id: contractID }, { state: "DISAGREE" });
      return true;
    },
    studentPayContract: async (parent, args, context, info) => {
      if (!context.user) {
        throw new AuthenticationError('Authentication token is invalid');
      }
      const user = await userSchema.findOne({ email: context.user.email });
      if (user.role !== 'STUDENT') {
        throw new Error('Phải là học sinh mới thực hiện được chức năng này!');
      }
      const { contractID } = args;
      const contract = await contractSchema.findOne({ _id: contractID });

      // Tính học phí
      const fee = +contract.feeHourly * +contract.timeStudy;
      if (user.money < fee) {
        throw new Error('Không đủ tiền!');
      }

      //Cập nhât lại
      //Tiền user
      await userSchema.updateOne({ email: context.user.email }, { money: +user.money - fee });
      //TT hợp đồng
      await contractSchema.updateOne({ _id: contractID }, { state: "AGREE" });

      return true;
    },
    studentComplaintContract: async (parent, args, context, info) => {
      if (!context.user) {
        throw new AuthenticationError('Authentication token is invalid');
      }
      const user = await userSchema.findOne({ email: context.user.email });
      if (user.role !== 'STUDENT') {
        throw new Error('Phải là học sinh mới thực hiện được chức năng này!');
      }
      const { contractID, studentMessage } = args;
      // const contract = await contractSchema.findOne({ _id: contractID });

      //Cap nhat trang thai contract
      await contractSchema.updateOne({ _id: contractID }, { state: "COMPLAINING" });

      //Tạo complaint
      await new complaintSchema({
        contractID,
        studentMessage,
        state: "PENDING",
        createAt: Date.now()
      }).save();

      return true;
    },
    studentFinishContract: async (parent, args, context, info) => {
      if (!context.user) {
        throw new AuthenticationError('Authentication token is invalid');
      }
      const user = await userSchema.findOne({ email: context.user.email });
      if (user.role !== 'STUDENT') {
        throw new Error('Phải là học sinh mới thực hiện được chức năng này!');
      }
      const { contractID, feedback } = args;
      const contract = await contractSchema.findOne({ _id: contractID });

      // Cập nhật hợp đồng
      await contractSchema.updateOne({ _id: contractID }, {
        state: "FINISH",
        finishAt: Date.now(),
        feedback: { ...feedback, createAt: Date.now() }
      })

      // Cập nhật rate
      await postSchema.updateOne({ teacherID: contract.teacherID }, { $push: { rate: feedback.rate } });

      // Chuyển tiền wa cho teacher
      const fee = +contract.feeHourly * +contract.timeStudy;

      const teacher = await userSchema.findOne({ _id: contract.teacherID });
      await userSchema.updateOne({ _id: contract.teacherID }, { money: +teacher.money + fee });

      return true;
    },
  },
};
