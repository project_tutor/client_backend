const userSchema = require('@schema/user');

module.exports = {
  Mutation: {
    editMoney: async (parent, args, context, info) => {
      if (!context.user) {
        throw new AuthenticationError('Authentication token is invalid');
      }
      const { money } = args;
      await userSchema.updateOne({ email: context.user.email }, { money: money })
      return true;
    }
  },

};
