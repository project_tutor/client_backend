const userSchema = require('@schema/user');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const transporter = require('@tools/nodemailer/index');

module.exports = {
  Mutation: {
    register: async (parent, args, context, info) => {
      const { fullname, email, password, role, isSocial, imageURL } = args.input;

      //check user đã tồn tại
      const listUsers = await userSchema.find({ email: email });
      if (listUsers.length > 0) {
        throw Error('Email đã tồn tại!');
      }

      //hash password
      const passwordHashed = await bcrypt.hash(
        password,
        +process.env.SALT_ROUNDS
      );

      //Save new user vao database
      const newUser = await new userSchema({
        fullname,
        email,
        password: passwordHashed,
        role,
        isActived: false,
        isReadEmail: false,
        isLocked: false,
        isSocial,
        imageURL
      }).save();

      if (newUser) {
        //Send Email
        const token = jwt.sign(
          {
            email: newUser.email
          },
          'your_jwt_secret'
        );
        transporter.sendMail({
          from: 'User Teacher vtanhxxx@gmail.com',
          to: email,
          subject: '⭐ Xác minh tài khoản Uber Teacher ⭐',
          html: `<p>Link: ${process.env.URL_FRONT_END}/confirm-email/${token}</p>`
        });
        return true;
      } else {
        throw Error('Tạo User thất bại!');
      }
    },

    confirmEmail: async (parent, args, context, info) => {
      const { token } = args;
      const user = jwt.verify(token, 'your_jwt_secret');
      await userSchema.updateOne({ email: user.email }, { isActived: true });
      return true;
    }
  }
};
