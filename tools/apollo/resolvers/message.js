const { PubSub, AuthenticationError } = require('apollo-server-express');
const pubsub = new PubSub();

const userSchema = require('@schema/user');
const messageSchema = require('@schema/message');
const roomChatSchema = require('@schema/roomChat');

module.exports = {
  Query: {
    myRoomChat: async (parent, args, context, info) => {
      if (!context.user) {
        throw new AuthenticationError('Authentication token is invalid');
      }

      const user = await userSchema.findOne({ email: context.user.email });
      let roomChat = await roomChatSchema.find({
        member: user._id
      });
      const myRoomChatReturn = []
      for (let i = 0; i < roomChat.length; ++i) {
        // Lay thông tin thành viên
        const memberIDInRoomNotMe = roomChat[i].member.filter((item) => item._id + "" !== user._id + "")
        const memberInRoomNotMe = await userSchema.find({ _id: { $in: memberIDInRoomNotMe } });

        // Lấy tất cả tin nhắn
        const allMessageInRoom = await messageSchema.find({ roomChatID: roomChat[i]._id });
        const allMessageInRoomReturn = [];
        for (let j = 0; j < allMessageInRoom.length; ++j) {
          const senderInMessage = await userSchema.findOne({ _id: allMessageInRoom[j].senderID });
          allMessageInRoomReturn.push({
            _id: allMessageInRoom[j]._id,
            roomChatID: allMessageInRoom[j].roomChatID,
            msg: allMessageInRoom[j].msg,
            createAt: allMessageInRoom[j].createAt,
            sender: senderInMessage
          });
        }

        myRoomChatReturn.push({
          _id: roomChat[i]._id,
          member: memberInRoomNotMe,
          allMessage: allMessageInRoomReturn
        })
      }
      return myRoomChatReturn;
    },
  },

  Mutation: {
    sendMessage: async (parent, args, context, info) => {
      const { receiverID, msg } = args;
      if (!context.user) {
        throw new AuthenticationError('Authentication token is invalid');
      }

      const sender = await userSchema.findOne({ email: context.user.email });
      const receiver = await userSchema.findOne({ _id: receiverID });

      let roomChat = await roomChatSchema.findOne({
        $and: [{ member: sender._id }, { member: receiver._id }]
      });
      roomChat = roomChat ? roomChat : await new roomChatSchema({ member: [sender._id, receiver._id] }).save();

      //Luu Message
      await new messageSchema(
        {
          roomChatID: roomChat._id,
          senderID: sender._id,
          msg,
          createAt: Date.now()
        }).save()

      // Xử lý onMessages
      roomChat.member.forEach(item => {
        if (sender._id + "" !== item + "") {
          pubsub.publish(`onMessages-${item}`, {
            onMessages: {
              roomChatID: roomChat._id,
              msg,
              sender: receiver
            }
          });
        }
      });

      return true;
    },
  },

  Subscription: {
    onMessages: {
      subscribe: async (parent, args, context, info) => {
        if (!context.user) {
          throw new AuthenticationError('Authentication token is invalid');
        }
        const user = await userSchema.findOne({ email: context.user.email });
        return pubsub.asyncIterator([`onMessages-${user._id}`]);
      }
    }
  }
};
