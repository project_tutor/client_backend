const userSchema = require('@schema/user');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const transporter = require('@tools/nodemailer/index');

module.exports = {
  Mutation: {
    forgotPassword: async (parent, args, context, info) => {
      const { email } = args;
      console.log(email);

      //  Send Email
      const token = jwt.sign(
        {
          email: email
        },
        'your_jwt_secret'
      );
      transporter.sendMail({
        from: 'User Teacher vtanhxxx@gmail.com',
        to: email,
        subject: '⭐ Reset mật khẩu Uber Teacher ⭐',
        html: `<p>Link: ${process.env.URL_FRONT_END}/forgetpassword/${token}</p>`
      });
      return true;
    },

    resetPassword: async (parent, args, context, info) => {
      const { token, newPassword } = args;
      const user = jwt.verify(token, 'your_jwt_secret');

      //hash password
      const passwordHashed = await bcrypt.hash(
        newPassword,
        +process.env.SALT_ROUNDS
      );

      await userSchema.updateOne({ email: user.email }, { password: passwordHashed });
      return true;
    }
  }
};
