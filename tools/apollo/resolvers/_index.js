const merge = require("lodash/merge");

const user = require("./user");
const login = require("./login");
const register = require("./register");
const forgot_password = require("./forgot-password");
const message = require("./message");
const tag = require("./tag");
const post = require("./post");
const file = require("./file");
const money = require("./money");
const contract = require("./contract");
const complaint = require("./complaint");
const home = require("./home");
const feedback = require("./feedback");

module.exports = merge(user, login, register, forgot_password, message, tag, post, money, file, contract, complaint, home, feedback);
