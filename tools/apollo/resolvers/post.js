const postSchema = require('@schema/post');
const userSchema = require('@schema/user');
const tagSchema = require('@schema/tag');
const contractSchema = require('@schema/contract');


module.exports = {
  Query: {
    myPost: async (parent, args, context, info) => {
      if (!context.user) {
        throw new AuthenticationError('Authentication token is invalid');
      }
      const user = await userSchema.findOne({ email: context.user.email });
      if (user.role !== "TEACHER") {
        throw new Error('Phải là teacher mới có chức năng này!');
      }
      const postOfUser = await postSchema.find({ teacherID: user._id });
      //TH user không co post
      if (postOfUser.length === 0) {
        return null;
      }

      // Join bảng tag(skill)
      const tags = await tagSchema.find({ _id: { $in: postOfUser[0].skill } });
      const postReturn = {
        _id: postOfUser[0]._id,
        teacher: user,
        title: postOfUser[0].title,
        overview: postOfUser[0].overview,
        hourlyRate: postOfUser[0].hourlyRate,
        education: postOfUser[0].education,
        employeeHistory: postOfUser[0].employeeHistory,
        skill: tags,
        rate: postOfUser[0].rate,
        // student
      };

      return postReturn;
    },
    posts: async (parent, args, context, info) => {
      const allposts = await postSchema.find({});
      const allpostsReturn = [];

      for (let i = 0; i < allposts.length; i++) {
        const tags = await tagSchema.find({ _id: { $in: allposts[i].skill } });
        const teacher = await userSchema.findOne({ _id: allposts[i].teacherID });
        allpostsReturn.push({
          _id: allposts[i]._id,
          teacher: teacher,
          title: allposts[i].title,
          overview: allposts[i].overview,
          hourlyRate: allposts[i].hourlyRate,
          education: allposts[i].education,
          employeeHistory: allposts[i].employeeHistory,
          skill: tags,
          rate: allposts[i].rate,
          // student
        });
      }

      return allpostsReturn;
    },
    followPost: async (parent, args, context, info) => {
      if (!context.user) {
        throw new AuthenticationError('Authentication token is invalid');
      }
      const user = await userSchema.findOne({ email: context.user.email });
      const result = [];
      for (let i = 0; i < user.follow.length; i++) {
        const followPostOfUser = await postSchema.find({ _id: { $in: user.follow[i] } });        
        result.push({_id: followPostOfUser[0]._id});
      }

      return result;
    },
    studiedPost: async (parent, args, context, info) => {
      if (!context.user) {
        throw new AuthenticationError('Authentication token is invalid');
      }
      const user = await userSchema.findOne({ email: context.user.email });
      const contract = await contractSchema.find({ studentID: user._id });
      const result = [];
      
      for (let i = 0; i < contract.length; i++) {
        const post = await postSchema.find({ teacher: { teacherID : {$in: contract.teacherID} }});
        if( contract[0].teacherID in result === false) {
          result.push({_id: contract[0].teacherID});
        }
      }
      return result;
    },
  },
  Mutation: {
    writePost: async (parent, args, context, info) => {
      if (!context.user) {
        throw new AuthenticationError('Authentication token is invalid');
      }
      const { title, overview, hourlyRate, skill, education, employeeHistory } = args.input;
      const user = await userSchema.findOne({ email: context.user.email });

      let arrIDTag = [];
      if (!!skill) {
        const arrSkill = skill.map((item) => item.name);
        const tagsCreated = await tagSchema.find({ name: { $in: arrSkill } });
        const tagsNotExist = arrSkill.filter(itemSkill => tagsCreated.findIndex(item => item.name === itemSkill) === -1);

        // Them các tag mới
        for (let i = 0; i < tagsNotExist.length; i++) {
          const tagAdd = await new tagSchema({ name: tagsNotExist[i], isAproved: false }).save();
          arrIDTag.push(tagAdd._id);
        }
        for (let i = 0; i < tagsCreated.length; i++) {
          arrIDTag.push(tagsCreated[i]._id);
        }
      }

      const postOfUser = await postSchema.find({ teacherID: user._id });
      if (postOfUser.length === 0) {
        // Add
        await postSchema({ teacherID: user._id, title, overview, hourlyRate, skill: arrIDTag, education, employeeHistory }).save();
      } else {
        // Edit
        await postSchema.updateOne({ teacherID: user._id }, { title, overview, hourlyRate, skill: arrIDTag, education, employeeHistory });
      }
      return true;
    },
    followingPost: async (parent, args, context, info) => {
      if (!context.user) {
        throw new AuthenticationError('Authentication token is invalid');
      }
      const { postId } = args;
      console.log(postId);
      
      const user = await userSchema.findOne({ email: context.user.email });

      const post = await postSchema.find({ _id: postId });
      if (post.length > 0) {
        // Add
        const newFollowPost = user.follow;
        newFollowPost.push(postId);
        await userSchema.updateOne({_id: user._id}, { follow: newFollowPost });
        return true;
      }
      else {
        throw new Error('Bản tin không tồn tại');
      }
    },
    cancelFollowPost: async (parent, args, context, info) => {
      if (!context.user) {
        throw new AuthenticationError('Authentication token is invalid');
      }
      const { postId } = args;
      
      const user = await userSchema.findOne({ email: context.user.email });

      const post = await postSchema.find({ _id: postId });
      if (post.length > 0) {
        // Add
        const newFollowPost = user.follow.filter(el => el != postId);
        await userSchema.updateOne({_id: user._id}, { follow: newFollowPost });
        return true;
      }
      else {
        throw new Error('Bản tin không tồn tại');
      }
    }
  }
};
