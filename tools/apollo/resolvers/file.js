var fs = require('fs');

module.exports = {
  Query: {
    uploads: async (parent, args, context, info) => { },
  },
  Mutation: {
    uploadFile: async (parent, args, context, info) => {
      return args.file.then(file => {
        // console.log(file);
        const { createReadStream, filename, mimetype } = file
        const fileStream = createReadStream()
        fileStream.pipe(fs.createWriteStream(`./images/${filename}`))
        //Contents of Upload scalar: https://github.com/jaydenseric/graphql-upload#class-graphqlupload
        //file.stream is a node stream that contains the contents of the uploaded file
        //node stream api: https://nodejs.org/api/stream.html
        return { ...file, path: `./images/${filename}` };
      });
    },
  },
};
