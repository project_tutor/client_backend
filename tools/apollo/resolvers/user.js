const { PubSub, AuthenticationError } = require('apollo-server-express');
const pubsub = new PubSub();
const fs = require('fs');
const userSchema = require('@schema/user');
const bcrypt = require('bcrypt');

module.exports = {
  Query: {
    users: async (parent, args, context, info) => {
      // if (!context.user) {
      //   throw new AuthenticationError('Authentication token is invalid');
      // }
      return await userSchema.find({});
    }
  },

  Mutation: {
    addUser: async (parent, args, context, info) => {
      try {
        const user = await new userSchema(args.input).save();
        pubsub.publish('USERS', {
          subUsers: user
        });
        return user;
      } catch (error) {
        return error;
      }
    },
    editUser: async (parent, args, context, info) => {
      const { userID, input, imageFile } = args;

      if (imageFile) {
        const { createReadStream, filename } = await imageFile;
        const fileStream = createReadStream();
        fileStream.pipe(fs.createWriteStream(`./public/images/${filename}`))
        await userSchema.updateOne({ _id: userID }, { ...input, imageURL: `/images/${filename}` });
      } else {
        await userSchema.updateOne({ _id: userID }, input);
      }

      return await userSchema.findOne({ _id: userID });
    },
    changePassword: async (parent, args, context, info) => {
      if (!context.user) {
        throw new AuthenticationError('Authentication token is invalid');
      }
      const { oldPassword, newPassword } = args;
      const user = await userSchema.findOne({ email: context.user.email });
      const checkPassword = await bcrypt.compare(oldPassword, user.password);
      if (!checkPassword) {
        throw new Error('Mật khẩu hiên tại không chính xác!');
      }
      if (oldPassword === newPassword) {
        throw new Error('Mật khẩu mới phải khác mật khẩu hiện tại!');
      }
      const passwordHashed = await bcrypt.hash(
        newPassword,
        +process.env.SALT_ROUNDS
      );
      await userSchema.updateOne({ _id: user._id }, { password: passwordHashed })

      return true
    }
  },

  Subscription: {
    subUsers: {
      subscribe: (parent, args, context, info) => {
        return pubsub.asyncIterator(['USERS']);
      }
    }
  }
};
