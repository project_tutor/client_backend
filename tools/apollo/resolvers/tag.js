const tagSchema = require('@schema/tag');

module.exports = {
  Query: {
    tags: async (parent, args, context, info) => {
      return await tagSchema.find();
    },
    tagsAproved: async (parent, args, context, info) => {
      return await tagSchema.find({ isAproved: true });
    }
  },
  Mutation: {
    addTag: async (parent, args, context, info) => {
      if (!context.user) {
        throw new AuthenticationError('Authentication token is invalid');
      }
      const { name } = args;
      await new tagSchema({ name, isAproved: false }).save();
      return true;
    }
  }
};
