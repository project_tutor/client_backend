const { PubSub, AuthenticationError } = require('apollo-server-express');
const pubsub = new PubSub();

const userSchema = require('@schema/user');
const messageSchema = require('@schema/message');

module.exports = {
  Mutation: {
    sendMessage: async (parent, args, context, info) => {
      const { receiverID, msg } = args;
      if (!context.user) {
        throw new AuthenticationError('Authentication token is invalid');
      }

      const sender = await userSchema.findOne({ email: context.user.email });
      const receiver = await userSchema.findOne({ _id: receiverID });
      await new messageSchema({ senderID: sender._id, receiverID: receiverID, msg: msg, createAt: Date.now() }).save();

      // Xử lý onMessages
      pubsub.publish(`onMessages-${receiver._id}`, {
        onMessages: {
          msg,
          sender: receiver
        }
      });

      return true;
    },
  },

  Subscription: {
    onMessages: {
      subscribe: async (parent, args, context, info) => {
        if (!context.user) {
          throw new AuthenticationError('Authentication token is invalid');
        }
        const user = await userSchema.findOne({ email: context.user.email });
        return pubsub.asyncIterator([`onMessages-${user._id}`]);
      }
    }
  }
};
