const postSchema = require('@schema/post');
const contractSchema = require('@schema/contract');
const userSchema = require('@schema/user');

module.exports = {
  Query: {
    getFeedbacksByPostID: async (parent, args, context, info) => {
      const { postID } = args;
      const post = await postSchema.findOne({ _id: postID });

      const allContract = await contractSchema.find({ teacherID: post.teacherID, state: 'FINISH' });
      const feedbacks = [];

      for (let i = 0; i < allContract.length; i++) {
        const student = await userSchema.findOne({ _id: allContract[i].studentID });
        feedbacks.push({ ...allContract[i].feedback, student });
      }

      return feedbacks;
    },
  },
};
